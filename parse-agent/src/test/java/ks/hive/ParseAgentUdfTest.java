package ks.hive;

import junit.framework.TestCase;
import org.apache.hadoop.io.Text;

import java.util.List;

/**
 * Created by ks on 11/19/15.
 */
public class ParseAgentUdfTest extends TestCase {

    ParseAgentUdf parser = new ParseAgentUdf();

    public void setUp() throws Exception {
        super.setUp();

    }

    public void tearDown() throws Exception {

    }

    public void testEvaluate() throws Exception {
        Text input = new Text("Mozilla/5.0 (Windows; U; Windows NT 6.0; en-US) AppleWebKit/532.0 (KHTML, like Gecko) Chrome/3.0.195.21 Safari/532.0");
        List<Text> result = parser.evaluate(input);
        assertEquals("Personal computer", result.get(0).toString());  // device
        assertEquals("Windows", result.get(1).toString());  // os
        assertEquals("Chrome", result.get(2).toString());  // browser
    }

    public void testEvaluateWrong() throws Exception {
        Text input = new Text("bla-bla");
        List<Text> result = parser.evaluate(input);
        assertEquals("", result.get(0).toString());
        assertEquals("unknown", result.get(1).toString());
        assertEquals("unknown", result.get(2).toString());
    }
}