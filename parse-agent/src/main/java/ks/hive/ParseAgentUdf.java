package ks.hive;

import net.sf.uadetector.ReadableUserAgent;
import net.sf.uadetector.UserAgentStringParser;
import net.sf.uadetector.service.UADetectorServiceFactory;
import org.apache.hadoop.hive.ql.exec.UDF;
import org.apache.hadoop.io.Text;

import java.util.List;

import static java.util.Arrays.asList;

/**
 * Created by Sergei_Korneev on 2015-11-18.
 */
public class ParseAgentUdf extends UDF {
    public static final String SEP = "\u0001";
    private Text device = new Text();
    private Text os = new Text();
    private Text browser = new Text();

    private UserAgentStringParser parser = UADetectorServiceFactory.getCachingAndUpdatingParser();

    public List<Text> evaluate(final Text s) {
        ReadableUserAgent userAgent = parser.parse(s.toString());
        device.set(userAgent.getDeviceCategory().getName());
        os.set(userAgent.getOperatingSystem().getFamilyName());
        browser.set(userAgent.getFamily().getName());
        return asList(device, os, browser);
    }
}