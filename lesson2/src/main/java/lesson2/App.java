package lesson2;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.*;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.io.compress.BZip2Codec;
import org.apache.log4j.Logger;

import java.io.*;
import java.util.*;

/**
 * Created by ks on 10/27/15.
 */
public class App {
    private static Logger logger = Logger.getLogger(App.class);
    private static Map<String, Long> counts = new HashMap<>();
    private static Path out;
    private static Path workfile1;
    private static Path workfile2;
    private static Path sortedWorkDir;
    private static List<Path> sortedFiles = new LinkedList<>();

    public static void main(String[] args) throws IOException {
        if (args.length != 3) {
            throw new IllegalArgumentException("Usage: App <input> <output> <workdir>");
        }

        Path in = new Path(args[0]);
        out = new Path(args[1]);
        Path workdir = new Path(args[2]);
        workfile1 = Path.mergePaths(workdir, new Path("/1"));
        workfile2 = Path.mergePaths(workdir, new Path("/2"));
        sortedWorkDir = Path.mergePaths(workdir, new Path("/sorted"));

        FileSystem fs = FileSystem.get(new Configuration());
        if (fs.exists(workdir)) {
            fs.delete(workdir, true);
        }
        fs.create(workfile1).close();

        // read files and process
        long n = 0;
        BZip2Codec codec = new BZip2Codec();
        codec.setConf(new Configuration());
        RemoteIterator<LocatedFileStatus> files = fs.listFiles(in, false);
        while (files.hasNext()) {
            LocatedFileStatus current = files.next();
            logger.info("Read file " + current.getPath().getName());
            BufferedReader reader = new BufferedReader(
                    new InputStreamReader(
                            codec.createInputStream(
                                    fs.open(current.getPath()))));
            String line = reader.readLine();
            while (line != null) {
                process(line, fs);
                line = reader.readLine();
                n++;
                if (n % 100000 == 0) {
                    logger.info("Processed " + n + " records. Size of map: " + counts.size());
                    if (!isMemoryEnough()) {
                        logger.info("Flush to disk");
                        mergeToDisk(fs);
                        counts.clear();
                    }
                }
            }
        }
        if (counts.size() > 0) {
            mergeToDisk(fs);
        }
        postprocess(fs);
        fs.delete(workdir, true);

    }

    private static Boolean isMemoryEnough() {
        long freeMemory = Runtime.getRuntime().freeMemory();
        logger.info("Free memory: " + freeMemory);
        return freeMemory > 20971520; // 20 MB
    }

    private static void process(String input, FileSystem fs) throws IOException {
        String id = parseInput(input);
        long n = counts.containsKey(id) ? counts.get(id) + 1 : 1;
        counts.put(id, n);
//        if (counts.size() > MAX_MAP_SIZE) {
//            mergeToDisk(fs);
//            counts.clear();
//        }
    }

    private static String parseInput(String input) {
        return input.split("\\t")[2];
    }

    private static void mergeToDisk(FileSystem fs) throws IOException {
        logger.info("Merging...");

        FSDataInputStream inputStream = fs.open(workfile1);
        FSDataOutputStream outputStream = fs.create(workfile2);
        Pair pair = new Pair();
        while (inputStream.available() > 0) {
            pair.readFields(inputStream);
            if (counts.containsKey(pair.key)) {
                pair.count += counts.get(pair.key);
                counts.remove(pair.key);
            }
            pair.write(outputStream);
        }
        inputStream.close();

        for (Map.Entry<String, Long> c : counts.entrySet()) {
            pair.key = c.getKey();
            pair.count = c.getValue();
            pair.write(outputStream);
        }
        outputStream.close();

        fs.delete(workfile1, true);
        fs.rename(workfile2, workfile1);
        counts.clear();

        logger.info("Merging done!");
    }

    private static void postprocess(FileSystem fs) throws IOException {
        logger.info("Sorting...");
        FSDataInputStream inputStream = fs.open(workfile1);
        List<Pair> values = new LinkedList<>();
        int i = 0;
        while (inputStream.available() > 0) {
            while (inputStream.available() > 0 && i < 500000) { // опытным путем для моего сандбокса
                Pair pair = new Pair();
                pair.readFields(inputStream);
                values.add(pair);
                i++;
            }
            flushSubList(fs, values);
            i = 0;
        }
        if (values.size() > 0) {
            flushSubList(fs, values);
        }

        mergeSorted(fs);
        logger.info("Sotring done.");
    }

    private static void flushSubList(FileSystem fs, List<Pair> list) throws IOException {
        logger.info("Flush sublist...");
        Collections.sort(list, new Comparator<Pair>() {
            @Override
            public int compare(Pair x, Pair y) {
                int i = x.count.compareTo(y.count);
                return i != 0 ? -i : x.key.compareTo(y.key);
            }
        });

        Path file = Path.mergePaths(sortedWorkDir, new Path(Path.SEPARATOR + UUID.randomUUID().toString()));
        FSDataOutputStream outputStream = fs.create(file);
        for (Pair p : list) {
            p.write(outputStream);
        }
        outputStream.close();
        list.clear();
        sortedFiles.add(file);
        logger.info("Flushing done");
    }

    private static void mergeSorted(final FileSystem fs) throws IOException {
        BufferedWriter resultWriter = new BufferedWriter(new OutputStreamWriter(fs.create(out)));
        List<DataInputStream> readers = new LinkedList<>();
        for (Path p : sortedFiles) {
            FSDataInputStream inputStream = fs.open(p);
            if (inputStream.available() > 0) {
                readers.add(inputStream);
                logger.info("Added file " + p.getParent() + "/" + p.getName());
            } else {
                logger.info("File " + p.getName() + " is empty. Did not added.");
            }
        }
        List<Map.Entry<DataInputStream, Pair>> minimalValues = new LinkedList<>();
        for (DataInputStream r : readers) {
            Pair pair = new Pair();
            pair.readFields(r);
            minimalValues.add(new AbstractMap.SimpleEntry<>(r, pair));
        }

        while (!minimalValues.isEmpty()) {
            Map.Entry<DataInputStream, Pair> min = Collections.min(minimalValues, new Comparator<Map.Entry<DataInputStream, Pair>>() {
                @Override
                public int compare(Map.Entry<DataInputStream, Pair> x, Map.Entry<DataInputStream, Pair> y) {
                    Pair xp = x.getValue();
                    Pair yp = y.getValue();
                    int i = xp.count.compareTo(yp.count);
                    return i != 0 ? -i : xp.key.compareTo(yp.key);
                }
            });

            min.getValue().repr(resultWriter);

            DataInputStream inputStream = min.getKey();
            if (inputStream.available() > 0) {
                min.getValue().readFields(inputStream);
            } else {
                minimalValues.remove(min);
                inputStream.close();
            }
        }
        resultWriter.close();
    }
}