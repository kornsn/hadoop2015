package lesson2;

import java.io.*;
import java.util.Objects;

/**
 * Created by ks on 10/28/15.
 */
public class Pair {
    public String key;
    public Long count;

    public Pair() {

    }

    public Pair(String key, Long count) {
        this.key = key;
        this.count = count;
    }

    public void readFields(DataInputStream in) throws IOException {
        key = in.readUTF();
        count = in.readLong();
    }

    public void write(DataOutputStream out) throws IOException {
        out.writeUTF(key);
        out.writeLong(count);
    }

    public void repr(BufferedWriter writer) throws IOException {
        writer
                .append(key)
                .append(';')
                .append(Long.toString(count));
        writer.newLine();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Pair pair = (Pair) o;
        return Objects.equals(key, pair.key) &&
                Objects.equals(count, pair.count);
    }

    @Override
    public int hashCode() {
        return Objects.hash(key, count);
    }
}
