package lesson4.mapreduce;

import lesson4.mapreduce.stage1.SplitKey;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.LocatedFileStatus;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.fs.RemoteIterator;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.MultipleOutputs;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;
import org.apache.log4j.Logger;

import java.net.URI;

/**
 * Driver.
 * Created by ks on 10/20/15.
 */
public class Driver extends Configured implements Tool {
    private static Logger logger = Logger.getLogger(Driver.class);

    public static void main(String[] args) throws Exception {
        System.exit(ToolRunner.run(new Configuration(), new Driver(), args));
    }

    @Override
    public int run(String[] args) throws Exception {
        // parse args
        if (args.length != 3) {
            throw new IllegalArgumentException("Usage: Driver <input> <temp> <output>");
        }

        Path in = new Path(args[0]);
        Path intermediateOut = new Path(args[1]);
        Path out = new Path(args[2]);

        Configuration conf = getConf();
        FileSystem fs = FileSystem.get(conf);
        fs.delete(out, true);
        fs.delete(intermediateOut, true);

        // job1 for stage 1
        Job job1 = Job.getInstance(conf, "Stage 1");
        job1.setJarByClass(getClass());
        job1.setMapperClass(lesson4.mapreduce.stage1.Mapper.class);
        job1.setCombinerClass(lesson4.mapreduce.stage1.Combiner.class);
        job1.setReducerClass(lesson4.mapreduce.stage1.Reducer.class);
        job1.setNumReduceTasks(1);
        job1.setInputFormatClass(TextInputFormat.class);
        job1.setOutputFormatClass(TextOutputFormat.class);
        job1.setMapOutputKeyClass(SplitKey.class);
        job1.setMapOutputValueClass(LongWritable.class);
        job1.setOutputKeyClass(Text.class);
        job1.setOutputValueClass(LongWritable.class);

        FileInputFormat.setInputPaths(job1, in);
        FileOutputFormat.setOutputPath(job1, intermediateOut);

        MultipleOutputs.addNamedOutput(job1, "values", TextOutputFormat.class, SplitKey.class, Text.class);

        if (!job1.waitForCompletion(true)) {
            // if first job is failed do not start stage 2
            return 1;
        }

        // job2 for stage 2
        Job job2 = Job.getInstance(conf, "Stage 2");
        job2.setJarByClass(getClass());
        job2.setMapperClass(lesson4.mapreduce.stage2.Mapper.class);
        job2.setNumReduceTasks(0);
        job2.setInputFormatClass(TextInputFormat.class);
        job2.setOutputFormatClass(TextOutputFormat.class);
        job2.setOutputKeyClass(LongWritable.class);
        job2.setOutputValueClass(Text.class);

        // add results of first job to cache
        RemoteIterator<LocatedFileStatus> fileStatuses = fs.listFiles(intermediateOut, false);
        while (fileStatuses.hasNext()) {
            LocatedFileStatus fileStatus = fileStatuses.next();
            URI uri = fileStatus.getPath().toUri();
            logger.info("Uri " + uri + " added to cache file.");
            job2.addCacheFile(uri);
        }

        FileInputFormat.setInputPaths(job2, Path.mergePaths(intermediateOut, new Path("/intermediateValues")));
        FileOutputFormat.setOutputPath(job2, out);

        return job2.waitForCompletion(true) ? 0 : 1;
    }
}
