package lesson4.mapreduce.stage1;

import org.apache.hadoop.io.LongWritable;

import java.io.IOException;

/**
 * Created by ks on 10/26/15.
 */
public class Combiner extends org.apache.hadoop.mapreduce.Reducer<SplitKey, LongWritable, SplitKey, LongWritable> {
    private LongWritable value = new LongWritable();

    @Override
    protected void reduce(SplitKey key, Iterable<LongWritable> values, Context context) throws IOException, InterruptedException {
        long n = 0;
        for (LongWritable v : values) {
            n += v.get();
        }
        value.set(n);
        context.write(key, value);
    }
}
