package lesson4.mapreduce.stage1;

import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.io.WritableComparable;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

/**
 * Created by ks on 10/26/15.
 */
public class SplitKey implements WritableComparable<SplitKey> {
    private Text splitKey;
    private LongWritable rowNumber;

    public SplitKey() {
        splitKey = new Text();
        rowNumber = new LongWritable();
    }

    public SplitKey(String splitKey) {
        this.splitKey = new Text(splitKey);
        this.rowNumber = new LongWritable(0);
    }

    public SplitKey(Text splitKey, LongWritable rowNumber) {
        this.splitKey = splitKey;
        this.rowNumber = rowNumber;
    }

    public Text getSplitKey() {
        return splitKey;
    }

    public void setSplitKey(Text splitKey) {
        this.splitKey = splitKey;
    }

    public LongWritable getRowNumber() {
        return rowNumber;
    }

    public void setRowNumber(LongWritable rowNumber) {
        this.rowNumber = rowNumber;
    }

    @Override
    public int compareTo(SplitKey splitKey) {
        return this.splitKey.compareTo(splitKey.splitKey);
    }

    @Override
    public void write(DataOutput out) throws IOException {
        splitKey.write(out);
        rowNumber.write(out);
    }

    @Override
    public void readFields(DataInput in) throws IOException {
        splitKey.readFields(in);
        rowNumber.readFields(in);
    }

    @Override
    public String toString() {
        return splitKey + "," + rowNumber;
    }
}
