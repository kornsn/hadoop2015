package lesson4.mapreduce.stage1;

import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;

import java.io.IOException;

/**
 * Created by ks on 10/26/15.
 */
public class Reducer extends org.apache.hadoop.mapreduce.Reducer<SplitKey, LongWritable, Text, LongWritable> {
    private long totalOffset = 0;
    private LongWritable totalOffsetWritable = new LongWritable();

    @Override
    protected void reduce(SplitKey key, Iterable<LongWritable> values, Context context) throws IOException, InterruptedException {
        totalOffsetWritable.set(totalOffset);
        context.write(key.getSplitKey(), totalOffsetWritable);
        long n = 0;
        for (LongWritable v : values) {
            n += v.get();
        }
        totalOffset += n;
    }
}
