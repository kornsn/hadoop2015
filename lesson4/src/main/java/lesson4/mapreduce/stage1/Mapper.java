package lesson4.mapreduce.stage1;

import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.lib.output.MultipleOutputs;

import java.io.IOException;
import java.util.UUID;

/**
 * Created by ks on 10/26/15.
 */
public class Mapper extends org.apache.hadoop.mapreduce.Mapper<LongWritable, Text, SplitKey, LongWritable> {
    private long totals = 0;
    private SplitKey splitKey = new SplitKey(UUID.randomUUID().toString());
    private MultipleOutputs<SplitKey, Text> multipleOutputs;

    @Override
    protected void setup(Context context) throws IOException, InterruptedException {
        multipleOutputs = new MultipleOutputs(context);
    }

    /**
     * Counts row number for each file.
     */
    @Override
    protected void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {
        totals++;
        multipleOutputs.write("values", splitKey, value, "intermediateValues/");
        splitKey.getRowNumber().set(splitKey.getRowNumber().get() + 1);
    }

    /**
     * Writes summary for each file: SplitKey as key and line count as value.
     */
    @Override
    protected void cleanup(Context context) throws IOException, InterruptedException {
        context.write(splitKey, new LongWritable(totals));
        multipleOutputs.close();
    }
}
