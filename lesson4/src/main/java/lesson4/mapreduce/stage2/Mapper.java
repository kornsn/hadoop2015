package lesson4.mapreduce.stage2;

import lesson4.mapreduce.stage1.SplitKey;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.log4j.Logger;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URI;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by ks on 10/26/15.
 */
public class Mapper extends org.apache.hadoop.mapreduce.Mapper<LongWritable, Text, LongWritable, Text> {

    private static Logger logger = Logger.getLogger(Mapper.class);

    private Map<String, Long> offsets = new HashMap<>();
    private SplitKey inKey = new SplitKey();
    private Text inValue = new Text();
    private LongWritable outKey = new LongWritable();

    @Override
    protected void setup(Context context) throws IOException, InterruptedException {
        // load map with offsets
        FileSystem fileSystem = FileSystem.get(context.getConfiguration());
        URI[] cacheFiles = context.getCacheFiles();
        String line;
        for (URI p : cacheFiles) {
            Path path = new Path(p.getPath());
            BufferedReader reader = new BufferedReader(new InputStreamReader(fileSystem.open(path)));
            line = reader.readLine();
            while (line != null) {
                readCache(line, offsets);
                line = reader.readLine();
            }
        }
        logger.debug("Cached offsets: " + offsets);
    }

    @Override
    protected void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {
        read(value.toString(), inKey, inValue);
        long offset = offsets.get(inKey.getSplitKey().toString());
        long rowNumber = inKey.getRowNumber().get() + offset;
        outKey.set(rowNumber);
        context.write(outKey, inValue);
    }

    private void read(String row, SplitKey outKey, Text outValue) {
        int i = row.indexOf(',');
        int i1 = row.indexOf('\t');
        outKey.getSplitKey().set(row.substring(0, i));
        outKey.getRowNumber().set(Long.parseLong(row.substring(i + 1, i1)));
        outValue.set(row.substring(i1 + 1));
    }

    private void readCache(String row, Map<String, Long> cache) {
        int i = row.indexOf('\t');
        cache.put(row.substring(0, i), Long.parseLong(row.substring(i + 1)));
    }
}
