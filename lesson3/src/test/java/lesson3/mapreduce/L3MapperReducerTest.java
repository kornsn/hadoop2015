package lesson3.mapreduce;

import lesson3.formats.MapResult;
import lesson3.formats.ReduceResult;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mrunit.mapreduce.MapDriver;
import org.apache.hadoop.mrunit.mapreduce.MapReduceDriver;
import org.apache.hadoop.mrunit.mapreduce.ReduceDriver;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;


/**
 * Map reduce test.
 * Created by ks on 10/22/15.
 */
public class L3MapperReducerTest {
    MapDriver<LongWritable, Text, Text, MapResult> mapDriver;
    L3Combiner combiner;
    ReduceDriver<Text, MapResult, Text, MapResult> combinerDriver;
    ReduceDriver<Text, MapResult, Text, ReduceResult> reduceDriver;
    MapReduceDriver<LongWritable, Text, Text, MapResult, Text, ReduceResult> mapReduceDriver;
    private Text mapInput1 = new Text("ip1 - - [24/Apr/2011:04:06:01 -0400] \"GET /~strabal/grease/photo9/927-3.jpg HTTP/1.1\" 200 40028 \"-\" \"Mozilla/5.0 (compatible; YandexImages/3.0; +http://yandex.com/bots)\"\n");
    private Text mapInput2 = new Text("ip1 - - [24/Apr/2011:04:06:01 -0400] \"GET /~strabal/grease/photo9/927-3.jpg HTTP/1.1\" 200 40028 \"-\" \"Mozilla2/5.0 (compatible; YandexImages/3.0; +http://yandex.com/bots)\"\n");

    @Before
    public void setUp() throws Exception {
        L3Mapper mapper = new L3Mapper();
        combiner = new L3Combiner();
        L3Reducer reducer = new L3Reducer();
        mapDriver = MapDriver.newMapDriver(mapper);
        combinerDriver = ReduceDriver.newReduceDriver(combiner);
        reduceDriver = ReduceDriver.newReduceDriver(reducer);
        mapReduceDriver = MapReduceDriver.newMapReduceDriver(mapper, reducer);
    }

    @Test
    public void testMapper() throws IOException {
        mapDriver
                .withInput(new LongWritable(), mapInput1)
                .withInput(new LongWritable(), mapInput2)
                .withInput(new LongWritable(), new Text("wrong input")) // test wrong row
                .withOutput(new Text("ip1"), new MapResult("ip1", 40028, 1, "Mozilla"))
                .withOutput(new Text("ip1"), new MapResult("ip1", 40028, 1, "Mozilla2"))
                .withCounter("Errors", "WrongInput", 1)
                .runTest();
    }

    @Test
    public void testCombiner() throws IOException {
        List<MapResult> values = new ArrayList<>();
        values.add(new MapResult("ip1", 10, 1, "Mozilla"));
        values.add(new MapResult("ip1", 20, 2, "Mozilla"));
        values.add(new MapResult("ip1", 30, 3, "Mozilla2"));
        combinerDriver
                .withInput(new Text("ip1"), values)
                .withOutput(new Text("ip1"), new MapResult("ip1", 60, 6, "Mozilla;Mozilla2"))
                .runTest();
    }

    @Test
    public void testReducer() throws IOException {
        List<MapResult> values = new ArrayList<>();
        values.add(new MapResult("ip1", 10, 1, "Mozilla;Mozilla2"));
        values.add(new MapResult("ip1", 20, 2, "Mozilla"));
        values.add(new MapResult("ip1", 30, 3, "Mozilla;Mozilla2;Mozilla3"));
        reduceDriver
                .withInput(new Text("ip1"), values)
                .withOutput(new Text("ip1"), new ReduceResult("ip1", 10, 60))
                .withCounter("Browsers", "Mozilla", 1)
                .withCounter("Browsers", "Mozilla2", 1)
                .withCounter("Browsers", "Mozilla3", 1)
                .runTest();
    }

    @Test
    public void testMapReduce() throws IOException {
        mapReduceDriver
                .withInput(new LongWritable(), mapInput1)
                .withInput(new LongWritable(), mapInput2)
                .withCombiner(combiner)
                .withOutput(new Text("ip1"), new ReduceResult("ip1", 40028, 80056))
                .withCounter("Browsers", "Mozilla", 1)
                .withCounter("Browsers", "Mozilla2", 1)
                .runTest();
    }
}
