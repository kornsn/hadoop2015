package lesson3.utils;

import lesson3.formats.Input;

import static org.junit.Assert.*;

/**
 * Created by ks on 10/22/15.
 */
public class ParserTest {
    Parser parser;
    Input input;

    @org.junit.Before
    public void setUp() throws Exception {
        parser = new Parser("(?<ip>ip\\w+).*\\\"GET.+\\\" \\d+ (?<bytes>\\d+).+\\\"[^\\\"]*\\\"\\s*\\\"\\W*(?<browser>\\w+)[^\\\"]*\\\".*");
        input = new Input();
    }

    @org.junit.After
    public void tearDown() throws Exception {

    }

    @org.junit.Test
    public void testParseSuccess() {
        String row = "ip1 - - [24/Apr/2011:04:06:01 -0400] \"GET /~strabal/grease/photo9/927-3.jpg HTTP/1.1\" 200 40028 \"-\" \"Mozilla/5.0 (compatible; YandexImages/3.0; +http://yandex.com/bots)\"";
        parser.tryParse(row, input);
        assertEquals(input.getIp(), "ip1");
        assertEquals(input.getBytes(), 40028);
        assertEquals(input.getBrowser(), "Mozilla");
    }

    @org.junit.Test
    public void testParseFail() {
        String row = "ip1 - - [24/Apr/2011:04:06:01 -0400] \"GET /~strabal/grease/photo9/927-3.jpg HTTP/1.1\" 200 a40028 \"-\" \"Mozilla/5.0 (compatible; YandexImages/3.0; +http://yandex.com/bots)\"";
        Boolean result = parser.tryParse(row, input);
        assertFalse(result);
    }
}