package lesson3.mapreduce;

import lesson3.formats.Input;
import lesson3.formats.MapResult;
import lesson3.formats.ReduceResult;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.io.compress.SnappyCodec;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.SequenceFileOutputFormat;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;

/**
 * Driver.
 * Created by ks on 10/20/15.
 */
public class L3Driver extends Configured implements Tool {
    public static void main(String[] args) throws Exception {
        System.exit(ToolRunner.run(new Configuration(), new L3Driver(), args));
    }

    @Override
    public int run(String[] args) throws Exception {
        // parse args
        if (args.length != 2) {
            throw new IllegalArgumentException("Usage: L3Driver <input> <output>");
        }

        Path in = new Path(args[0]);
        Path out = new Path(args[1]);

        Configuration conf = getConf();
//        conf.set(SequenceFileOutputFormat.COMPRESS_CODEC, "GzipCodec");

        FileSystem.newInstance(conf).delete(out, true);
        Job job = Job.getInstance(conf);
        job.setJarByClass(getClass());
        job.setMapperClass(L3Mapper.class);
        job.setCombinerClass(L3Combiner.class);
        job.setReducerClass(L3Reducer.class);
        job.setInputFormatClass(TextInputFormat.class);
        job.setOutputFormatClass(SequenceFileOutputFormat.class);
        job.setMapOutputKeyClass(Text.class);
        job.setMapOutputValueClass(MapResult.class);
        job.setOutputKeyClass(Text.class);
        job.setOutputValueClass(ReduceResult.class);

        FileInputFormat.setInputPaths(job, in);
        SequenceFileOutputFormat.setOutputPath(job, out);
        SequenceFileOutputFormat.setCompressOutput(job, true);
        SequenceFileOutputFormat.setOutputCompressorClass(job, SnappyCodec.class);

        if (job.waitForCompletion(true)) {
            return 0;
        } else {
            return 1;
        }
    }
}
