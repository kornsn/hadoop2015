package lesson3.mapreduce;

import lesson3.formats.Input;
import lesson3.formats.MapResult;
import lesson3.utils.Parser;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.io.Writable;
import org.apache.hadoop.mapreduce.Mapper;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.io.IOException;

/**
 * Mapper.
 * Created by ks on 10/20/15.
 */
public class L3Mapper extends Mapper<LongWritable, Text, Text, MapResult> {

    private Input input = new Input();
    private MapResult result = new MapResult();
    private Text id = new Text();

    private Parser parser;

    @Override
    protected void setup(Context context) throws IOException, InterruptedException {
        ApplicationContext applicationContext = new ClassPathXmlApplicationContext("config.xml");
        parser = (Parser) applicationContext.getBean("parser");
    }

    @Override
    protected void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {
        if (parser.tryParse(value.toString(), input)) {
            id.set(input.getIp());
            result.setIp(input.getIp());
            result.setBytes(input.getBytes());
            result.setCount(1);
            result.setBrowsers(input.getBrowser());
            context.write(id, result);
        } else {
            context.getCounter("Errors", "WrongInput").increment(1);
        }
    }
}
