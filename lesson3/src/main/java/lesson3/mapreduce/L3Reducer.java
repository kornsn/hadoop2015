package lesson3.mapreduce;

import lesson3.formats.MapResult;
import lesson3.formats.ReduceResult;
import org.apache.commons.lang.StringUtils;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

import java.io.IOException;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

/**
 * Reducer.
 * Created by ks on 10/20/15.
 */
public class L3Reducer extends Reducer<Text, MapResult, Text, ReduceResult> {
    ReduceResult result = new ReduceResult();

    @Override
    protected void reduce(Text key, Iterable<MapResult> values, Context context) throws IOException, InterruptedException {
        int count = 0;
        long totals = 0;
        float avg;
        Set<String> browsers = new HashSet<>();
        for (MapResult v : values) {
            count += v.getCount();
            totals += v.getBytes();
            // add browsers
            Collections.addAll(browsers, StringUtils.split(v.getBrowsers(), ';'));
        }
        avg = totals / (float) count;
        result.setAvg(avg);
        result.setIp(key.toString());
        result.setTotals(totals);
        context.write(key, result);
        for (String browser : browsers) {
            context.getCounter("Browsers", browser).increment(1);
        }
    }
}
