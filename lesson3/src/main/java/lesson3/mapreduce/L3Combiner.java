package lesson3.mapreduce;

import lesson3.formats.MapResult;
import org.apache.commons.lang.StringUtils;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

/**
 * Combiner.
 * Created by ks on 10/22/15.
 */
public class L3Combiner extends Reducer<Text, MapResult, Text, MapResult> {
    private MapResult result = new MapResult();

    @Override
    protected void reduce(Text key, Iterable<MapResult> values, Context context) throws IOException, InterruptedException {
        int count = 0;
        long totals = 0;
        Set<String> browsers = new HashSet<>();
        for (MapResult v : values) {
            count += v.getCount();
            totals += v.getBytes();
            browsers.add(v.getBrowsers());
        }
        result.setIp(key.toString());
        result.setBytes(totals);
        result.setCount(count);
        result.setBrowsers(StringUtils.join(browsers, ';'));
        context.write(key, result);
    }
}
