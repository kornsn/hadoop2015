package lesson3.formats;

import org.apache.hadoop.io.FloatWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.io.Writable;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import java.util.Objects;

/**
 * Reducer result.
 * Created by ks on 10/20/15.
 */
public class ReduceResult implements Writable {
    Text ip = new Text();
    FloatWritable avg = new FloatWritable();
    LongWritable totals = new LongWritable();

    public ReduceResult() {
    }

    public ReduceResult(String ip, float avg, long totals) {
        setIp(ip);
        setAvg(avg);
        setTotals(totals);
    }

    public String getIp() {
        return ip.toString();
    }

    public void setIp(String ip) {
        this.ip.set(ip);
    }

    public float getAvg() {
        return avg.get();
    }

    public void setAvg(float avg) {
        this.avg.set(avg);
    }

    public long getTotals() {
        return totals.get();
    }

    public void setTotals(long totals) {
        this.totals.set(totals);
    }

    public void write(DataOutput dataOutput) throws IOException {
        ip.write(dataOutput);
        avg.write(dataOutput);
        totals.write(dataOutput);
    }

    public void readFields(DataInput dataInput) throws IOException {
        ip.readFields(dataInput);
        avg.readFields(dataInput);
        totals.readFields(dataInput);
    }

    @Override
    public String toString() {
        return avg + "," + totals;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ReduceResult result = (ReduceResult) o;
        return Objects.equals(ip, result.ip) &&
                Objects.equals(avg, result.avg) &&
                Objects.equals(totals, result.totals);
    }

    @Override
    public int hashCode() {
        return Objects.hash(ip, avg, totals);
    }
}
