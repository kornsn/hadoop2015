package lesson3.formats;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.io.Writable;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import java.util.Objects;

/**
 * Input data.
 * Created by ks on 10/20/15.
 */
public class Input {
    private String ip;
    private int bytes;
    private String browser;

    public Input() {
    }

    public Input(String ip, int bytes, String browser) {
        this.ip = ip;
        this.bytes = bytes;
        this.browser = browser;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public int getBytes() {
        return bytes;
    }

    public void setBytes(int bytes) {
        this.bytes = bytes;
    }

    public String getBrowser() {
        return browser;
    }

    public void setBrowser(String browser) {
        this.browser = browser;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Input input = (Input) o;
        return Objects.equals(ip, input.ip) &&
                Objects.equals(bytes, input.bytes) &&
                Objects.equals(browser, input.browser);
    }

    @Override
    public int hashCode() {
        return Objects.hash(ip, bytes, browser);
    }
}
