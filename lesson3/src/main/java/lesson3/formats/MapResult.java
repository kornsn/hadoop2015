package lesson3.formats;

import org.apache.commons.lang.StringUtils;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.io.Writable;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import java.util.Collections;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

/**
 * Result of mapper.
 * Created by ks on 10/22/15.
 */
public class MapResult implements Writable {
    /**
     * Ip
     */
    private Text ip = new Text();

    /**
     * Subtotals of bytes
     */
    private LongWritable bytes = new LongWritable();

    /**
     * Count of processed requests
     */
    private IntWritable count = new IntWritable();

    /**
     * Semicolon separated list of browsers
     */
    private Text browsers = new Text();

    public MapResult() {
    }

    public MapResult(String ip, long bytes, int count, String browsers) {

        setIp(ip);
        setBytes(bytes);
        setCount(count);
        setBrowsers(browsers);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MapResult mapResult = (MapResult) o;
        return Objects.equals(ip, mapResult.ip) &&
                Objects.equals(bytes, mapResult.bytes) &&
                Objects.equals(count, mapResult.count) &&
                Objects.equals(getBrowsersAsSet(), mapResult.getBrowsersAsSet()); // this method is used in tests only, so it's normal
    }

    @Override
    public int hashCode() {
        return Objects.hash(ip, bytes, count, getBrowsersAsSet()); // this method is used in tests only, so it's normal
    }

    public String getIp() {
        return ip.toString();
    }

    public void setIp(String ip) {
        this.ip.set(ip);
    }

    public long getBytes() {
        return bytes.get();
    }

    public void setBytes(long bytes) {
        this.bytes.set(bytes);
    }

    public int getCount() {
        return count.get();
    }

    public void setCount(int count) {
        this.count.set(count);
    }

    public String getBrowsers() {
        return browsers.toString();
    }

    public void setBrowsers(String browsers) {
        this.browsers.set(browsers);
    }

    @Override
    public void write(DataOutput dataOutput) throws IOException {
        ip.write(dataOutput);
        bytes.write(dataOutput);
        count.write(dataOutput);
        browsers.write(dataOutput);
    }

    @Override
    public void readFields(DataInput dataInput) throws IOException {
        ip.readFields(dataInput);
        bytes.readFields(dataInput);
        count.readFields(dataInput);
        browsers.readFields(dataInput);
    }

    @Override
    public String toString() {
        return "MapResult{" +
                "ip=" + ip +
                ", bytes=" + bytes +
                ", count=" + count +
                ", browsers=" + browsers +
                '}';
    }

    private Set<String> getBrowsersAsSet() {
        Set<String> set = new HashSet<>();
        Collections.addAll(set, StringUtils.split(getBrowsers(), ';'));
        return set;
    }
}
