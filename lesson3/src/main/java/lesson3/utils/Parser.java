package lesson3.utils;

import lesson3.formats.Input;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Class for parsing input rows.
 * Created by ks on 10/20/15.
 */
public class Parser {
    private Pattern regexp;

    /**
     * @param regexp Regular expression. Must contain named groups 'ip', 'bytes', 'browser'.
     */
    public Parser(String regexp) {
        this.regexp = Pattern.compile(regexp);
    }

    public Boolean tryParse(String row, Input input) {
        Matcher matcher = regexp.matcher(row);
        Boolean success = matcher.find();
        if (!success) {
            return false;
        }

        input.setIp(matcher.group("ip"));
        input.setBytes(Integer.parseInt(matcher.group("bytes")));
        input.setBrowser(matcher.group("browser"));
        return true;
    }
}
